<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Cours 01 Foreach</title>
</head>

<body>
<pre>
<?php

$myVariable = "Jeff";
echo $myVariable . "<br />";

$myVariable = "Joe";
echo $myVariable . "<br />";

$myVariable = "Keith";
echo $myVariable . "<br />";

?>

<?php

$myVariable = array("Jeff", "Joe", "Keith");
foreach($myVariable as $item) {
	echo $item . "<br />";
}


?>

</pre>
</body>
</html>