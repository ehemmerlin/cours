<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Cours 01 Array</title>
</head>

<body>
	Etape 1 : Création d'un tableau
	<pre><?php
		// Création d'un tableau indexé numériquement : 1ère méthode
		$membre = array('Alain','Marc','Thierry');
		print_r($membre);
		//var_dump($membre);
		echo "\n"; // Aller à la ligne

		//Accès à un élément du tableau
		echo '$membre[1]='.$membre[1]; // Affiche Marc car l'index des tableaux commence à 0
		echo "\n\n"; // Aller à la ligne

		// Libération de la variable $membre
		unset($membre);

		// Création d'un tableau indexé numériquement : 2ème méthode
		$membre = array();
		$membre[] = 'Alain';
		$membre[] = 'Marc';
		$membre[] =  'Thierry';
		print_r($membre);
		//var_dump($membre);
		//var_export($membre);
		echo "\n"; // Aller à la ligne

		//Accès à un élément du tableau
		echo '$membre[0]='.$membre[0]; // Affiche Alain car l'index des tableaux commence à 0
		echo "\n\n"; // Aller à la ligne

		// Libération de la variable $membre
		unset($membre);

		// Création d'un tableau associatif : 1ère méthode
		$membre = array('etudiant1' =>'Alain', 'etudiant2' =>'Marc', 'etudiant3' =>'Thierry');
		print_r($membre);

		//Accès à un élément du tableau
		echo '$membre[\'etudiant1\']='.$membre['etudiant1'];
		echo "\n\n"; // Aller à la ligne

		// Libération de la variable $membre
		unset($membre);

		// Création d'un tableau associatif : 2ème méthode
		$membre = array();
		$membre['etudiant1'] = 'Alain';
		$membre['etudiant2'] = 'Marc';
		$membre['etudiant3'] = 'Thierry';
		print_r($membre);

		//Accès à un élément du tableau
		echo '$membre[\'etudiant1\']='.$membre['etudiant1'];
		echo "\n\n"; // Aller à la ligne

		// Libération de la variable $membre
		unset($membre);
		?>
	</pre>
	Etape 2 : Parcourir les éléments d'un tableau
	<p><?php
		// Création d'un tableau indexé numériquement
		$membre = array('Alain','Marc','Thierry');

		// Avec une boucle for
		for ($i=0; $i<count($membre); $i++){
			echo $membre[$i], "<br>";
		}

		// Avec une boucle while
		$i=0;
		while ($i<count($membre)){
			echo $membre[$i], "<br>";
			$i++;
		}

		// Avec une boucle foreach
		foreach($membre as $val) {
			echo "$val<br>";
		}

		unset($membre);

		// Création d'un tableau associatif
		$membre = array('etudiant1' =>'Alain', 'etudiant2' =>'Marc', 'etudiant3' =>'Thierry');

		// Avec une boucle foreach et indication de la cle
		foreach($membre as $cle=>$val) {
		echo "$cle est $val<br>";
		}
		?>
	</p>
	Etape 3 : tableaux multi-dimensionnels
	<pre><?php
		$theVariable = array("Search Engines" =>
				array (
						0=> "http//google.com",
						1=> "http//yahoo.com",
						2=> "http//msn.com/"),
				"Social Networking Sites" =>
				array (
						0 => "http//www.facebook.com",
						1 => "http//www.myspace.com",
						2 => "http//vkontakte.ru",)
		);
		print_r($theVariable);
		echo "La première valeur du tableau est " . $theVariable['Search Engines'][0];
		// La première valeur du tableau est http://google.com
		?>
	</pre>
	Etape 4 : tri des tableaux
	<pre><?php
		// Fonction ksort() : tri sur les clés ascendantes
		$tab2 = array ("1802"=>"Hugo","1622"=>"Molière","1920"=>"Vian");
		echo "Tri utilisant la fonction ksort()\n";
		echo "Avant tri sur les clés de \$tab2\n";
		foreach ($tab2 as $cle=>$valeur) {
			echo "l’élément a pour clé : $cle et pour valeur : $valeur <br />";
		}
		ksort ($tab2);
		echo "Après tri sur les clés de \$tab2\n";
		foreach ($tab2 as $cle=>$valeur) {
			echo "l’élément a pour clé : $cle et pour valeur : $valeur <br />";
		}
		// Fonction arsort() : tri sur les valeurs descendantes
		$tab2 = array ("1802"=>"Hugo","1622"=>"Molière","1920"=>"Vian");
		echo "\nTri utilisant la fonction arsort()\n";
		echo "Avant tri sur les clés de \$tab2\n";
		foreach ($tab2 as $cle=>$valeur) {
			echo "l’élément a pour clé : $cle et pour valeur : $valeur <br />";
		}
		arsort ($tab2);
		echo "Après tri sur les clés de \$tab2\n";
		foreach ($tab2 as $cle=>$valeur) {
		echo "l’élément a pour clé : $cle et pour valeur : $valeur <br />";
		}
		?>
	</pre>
	Etape 5 : tri des tableaux (avancé)
	<pre><?php
		// Fonction array_filter()
		function impair($var)
		{
			return ($var % 2 == 1);
		}
		function pair($var)
		{
			return ($var % 2 == 0);
		}
		$array1 = array ("a"=>1, "b"=>2, "c"=>3, "d"=>4, "e"=>5);
		$array2 = array (6, 7, 8, 9, 10, 11, 12);
		echo "Impairs :\n";
		print_r(array_filter($array1, "impair"));
		echo "Pairs :\n";
		print_r(array_filter($array2, "pair"));
		?>
	</pre>
	</body>
</html>
