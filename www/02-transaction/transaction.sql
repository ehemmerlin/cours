START TRANSACTION;

	DELETE FROM LigneCommandes
	WHERE CommandeId IN (
		SELECT CommandeId
		FROM Commande
		WHERE ClientId = 'ALFKI');

	DELETE FROM Commande
	WHERE ClientId = 'ALFKI';

	DELETE FROM Client
	WHERE ClientId = 'ALFKI';

ROLLBACK;

COMMIT;

/*
-Atomique
-Cohérente
-Isolée
-Durable
*/

