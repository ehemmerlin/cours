<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Cours 01 Is</title>
</head>
<body>
	<?php
	$s = "test";
	echo isset($s); // Renvoie TRUE
	echo isset($j); // Renvoie FALSE
	?>
	<br>
	<?php
	$s = "test";
	echo isset($s); // Renvoie TRUE
	unset($s);
	echo isset($s); // Renvoie FALSE
	?>
</body>
</html>
