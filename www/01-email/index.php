<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Cours 01 Email</title>
</head>
<body>
<?php
// Envoi d'un message
$adr = 'superman@me.com';
mail($adr, 'Sujet', "Texte du message\nLigne 2\n");

// Envoi d'un message à plusieurs personnes
$destinataires = 'superman@me.com,superwoman@me.com';
// On sépare les destinataires par une virgule.
$sujet = 'Titre du message';
mail($destinataires, $sujet, "Texte\nLigne 2");

// Envoi d'un message comportant un header spécifique
$destinataires = 'superman@me.com,superwoman@me.com';
$sujet = 'Vous n\'avez pas réglé vos cotisations';
$entetes = "From: responsable@urssaf.fr \n";
$entetes .= "Reply-to: adresseretour@urssaf.fr\n";
//$entetes .= "Cc: secretaire@urssaf.fr \n";
//$entetes .= "Bcc: contentieux@urssaf.fr \n";
/* Ici notre message sera envoyé en copie à secretaire@urssaf.fr
 et en copie cachée à contentieux@urssaf.fr */
$ret = mail($destinataires, $sujet, "Texte\nLigne 2",$entetes);
echo $ret ? "Le message a bien été envoyé." : "Le message n'a pas pu être envoyé."
?>
</body>
</html>