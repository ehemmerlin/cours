<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Cours 01 Boucle</title>
</head>
<body>
	<p>
		<?php
		// Structure de contrôle Whiile
		$i = 1;
		while ( $i <= 10 )
		{
			echo $i;
			$i++;
		}
		?>
		<br>
		<?php
		// Structure de contôle For
		for ( $i = 2; $i <= 10 ; $i++ ) {
			echo "$i -";
		}
		?>
		<br>
		<?php
		// Structure de contôle Foreach
		$tab = array(
				'prenom' => 'Cyril' ,
				'ville' => 'Paris' ,
				'travail' => 'informatique'
		);
		foreach ($tab as $element) {
			echo "Valeur: $element<br>\n";
		}
		foreach ($tab as $cle => $valeur) {
			echo "Cle : $cle; Valeur: $valeur<br>\n";
		}
		?>
		<br>
		<?php
		// Structure de contôle Foreach utilisant les valeurs par références et non par copie (depuis PHP 5)
		$tab = array(1, 2, 3) ;
		foreach($tab as $valeur) {
			$valeur = $valeur-1;
		}
		echo $tab[0] + $tab[1] + $tab[2] ; // Affiche 6
		foreach($tab as &$valeur) {
			$valeur = $valeur-1;
		}
		echo $tab[0] + $tab[1] + $tab[2] ; // Affiche 3
		?>
		<br>

	</p>
</body>
</html>
