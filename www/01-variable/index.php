<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Cours 01 Variable</title>
</head>
<body>
	<?php
	// Variables statiques
	$myVariable = 'Jeff';

	# echo "Hello, $myVariable";
	# echo 'Hello, ' . $myVariable . '.';

	if ($myVariable == 'Jeff') {
		print 'Votre prénom est Jeff.';
	} else {
		print "Votre prénom n'est pas Jeff.";
	}

	?>
	<br>
	<?php
	// Variables dynamiques
	$cd = "15 _";
	$dvd = "30 _";
	$produit = "dvd"; // On choisit comme produit le dvd
	echo $$produit; // Affiche 30 _ soit le prix du dvd
	?>
	<br>
	<?php
	//Il est aussi possible de référencer un nom dynamique en une seule opération grâce à des accolades :
	$dvd = "30 _";
	echo ${"dvd"}; // Affiche 30 _
	$produit = "dvd";
	echo ${$produit}; // Équivaut à echo $dvd et affiche donc 30 _
	?>
	<br>
	<?php
	// Il est possible de faire des opérations à l’intérieur des accolades, par exemple des concaténations.
	$cd = "15 _";
	$dvd = "30 _";
	echo ${"d" . "vd"}; // Affiche 30 _
	?>
</body>
</html>
