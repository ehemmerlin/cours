<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Cours 01 Filter</title>
</head>
<body>
	<?php 
	// Données à valider
	$entier1 = 12;
	$entier2 = 21;
	// Intervalle de validation
	$min = 20;
	$max = 30;
	
	// Filtrage
	echo "Validation de l'entier '$entier1' ";
	echo "dans l'intervalle ($min, $max) : \n";
	var_dump(filter_var($entier1, FILTER_VALIDATE_INT,
			array('options' =>
					array('min_range' => $min, 'max_range' => $max))
	)
	);
	
	echo "Validation de l'entier $entier2 dans l'intervalle " ;
	echo "( $min , $max )\n";
	var_dump(filter_var($entier2, FILTER_VALIDATE_INT,
			array('options' =>
					array('min_range' => $min, 'max_range' => $max))
	)
	);
	?>
</body>
</html>
