<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Cours 01 Filter</title>
</head>
<body>
	<form method='POST' action='index.php'>
		<input type=text name="mel" value=""> 
		<input type="Submit" name="submit" value="Soumettre la requête">
	</form>
	<?php
	// Validation
	if (isset($_POST['submit'])) {
		$mail = filter_input(INPUT_POST, 'mel', FILTER_VALIDATE_EMAIL) ;
		if ($mail === FALSE){
			echo 'Le mail fourni n\'est pas valide' ;
		}elseif($mail === NULL){
			echo 'mel n\'était pas définie.' ;
		}else{
			echo "La variable est une adresse e-mail valide : $mail";
		}
	}
	?>
</body>
</html>
