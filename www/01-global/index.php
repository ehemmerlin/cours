<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Cours 01 Global</title>
</head>
<body>
	<p>
		<?php
		function affiche1($var1,$var2){
			echo $var1, $GLOBALS['titre1'], $var2, $GLOBALS['auteur1'];
		}
		$titre1 = 'EL DESDICHADO';
		$auteur1 = 'Gérard de Nerval';
		affiche1('Mon poème préféré est ', '.<br> Son auteur est ');
		// Affiche Mon poème préféré est EL DESDICHADO.
		// Son auteur est Gérard de Nerval
		?>
		<br>
		<?php
		function affiche2($var1,$var2){
			global $titre2, $auteur2 ;
			echo $var1, $titre2, $var2, $auteur2;
		}
		$titre2 = 'EL DESDICHADO';
		$auteur2 = 'Gérard de Nerval';
		affiche2('Mon poème préféré est ', '.<br>Son auteur est ');
		// Affiche Mon poème préféré est EL DESDICHADO.
		// Son auteur est Gérard de Nerval
		?>
	</p>
</body>
</html>
