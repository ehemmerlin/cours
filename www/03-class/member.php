<?php
/**
 * Classe Member
 * @author HB
 *
 */
class Member
{
	/**
	 * Attribut nom du membre
	 * @var string
	 */
	public $username = "";

	/**
	 * Attribut indiquant si le membre est loggé
	 * @var bool
	 */
	private $loggedIn = false;

	/**
	 * Méthode loggin()
	 */
	public function login() {
		$this->loggedIn = true;
	}
	
	/**
	 * Méthode logout
	 */
	public function logout() {
		$this->loggedIn = false;
	}
	
	/**
	 * Méthode isLoggedIn
	 */
	public function isLoggedIn() {
		return $this->loggedIn;
	}
}
?>