<?php 
require_once "member.php";
?>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
</head>
<body>
<?php 
$member = new Member();
$member->username = "Fred";
echo $member->username . " est " . ( $member->isLoggedIn() ? "connecté" : "déconnecté" ) . "<br>";
$member->login();
echo $member->username . " est " . ( $member->isLoggedIn() ? "connecté" : "déconnecté" ) . "<br>";
$member->logout();
echo $member->username . " est " . ( $member->isLoggedIn() ? "connecté" : "déconnecté" ) . "<br>";
?>
</body>
</html>