<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Cours 01 Affectation</title>
</head>
<body>
	<p><?php
		// Affectation par valeur
		$origine = 1 ;
		$copie = $origine ;
		$copie = $origine + 1 ;
		echo $copie ; // Donne 2 (1+1)
		echo $origine ; // Donne 1 (sa valeur n’a pas été modifiée)
		?>
		<br>
		<?php
		// Affectation par référence
		$origine = 1 ;
		$copie =& $origine ;
		$copie = $copie +1;
		echo $copie ; // Donne 2 (1+1)
		echo $origine ; // Donne 2 aussi
		// Puisque les deux noms correspondent en fait à la même valeur
		?>
		<br>
		<?php
		// Effacer une références
		$origine = 1 ;
		$reference =& $origine ;
		$origine = 2 ;
		echo 'Valeur de $reference : ', $reference, '<br>'; // Affiche 2
		unset($origine) ;
		echo 'Valeur de $origine : ', $origine, '<br>'; // Affiche une erreur
		echo 'Valeur de $reference : ', $reference, '<br>'; // Affiche toujours la valeur 2
		?>
	</p>
</body>
</html>
