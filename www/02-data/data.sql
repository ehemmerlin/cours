-- Les types de données
-- Introdution aux outils suivants :
--   INT, VARCHAR, CHAR, DATE, etc.


-- Création de la base de donnée cours
CREATE DATABASE IF NOT EXISTS cours;

-- Placement sur la base de donnée cours
USE cours;

-- Suppression de la table client
DROP TABLE IF EXISTS client;

-- Création de la table client
CREATE TABLE IF NOT EXISTS client (
    cli_id INT(4) ZEROFILL NOT NULL AUTO_INCREMENT,
    cli_int TINYINT NOT NULL,
    cli_varchar VARCHAR(3) NOT NULL,
    cli_char CHAR(3) NOT NULL,
    cli_date DATE NOT NULL,
    cli_time TIME NOT NULL,
    PRIMARY KEY (cli_id)
)  ENGINE=InnoDB DEFAULT CHARACTER SET=utf8 COLLATE = utf8_general_ci; -- ENGINE=InnoDB -- ENGINE=MEMORY -- ENGINE=MyISAM

-- Insertion de données dans la table client
INSERT INTO `cours`.`client` 
VALUES (1, 11111, 'PARIS', 'tennis', '2010/01/01', '01:01:01');

-- Affichage des données insérées
SELECT 
    *
FROM
    client ; -- ORDER BY cli_categorie;

-- Détaille la structure de la table client
DESCRIBE client;