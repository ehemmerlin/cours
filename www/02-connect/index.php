<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Cours 02 Connect</title>
</head>
<body>
<pre>
<?php 
/*
  -- Structure de la table
  CREATE TABLE IF NOT EXISTS `contact` (
  `con_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `con_nom` varchar(255) NOT NULL,
  `con_email` varchar(255) NOT NULL,
  `con_message` text NOT NULL,
  PRIMARY KEY (`con_id`)
  ) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

  -- Contenu de la table
  INSERT INTO `contact` (`con_id`, `con_nom`, `con_email`, `con_message`) VALUES
  (1, 'Pierre Martin', 'pm@gmail.com', 'Super site');
 */

$host			= 'localhost';		// NOM DU SERVEUR SQL
$user			= 'root';			// VOTRE NOM D'UTILISATEUR SQL
$pass			= '';				// VOTRE MOT DE PASSE SQL
$database       = 'cv';		// NOM DE VOTRE BASE DE DONNEES

// Connexion à MySql
$connex = mysql_connect($host , $user, $pass);
if (!$connex) {
	die('Non connect&eacute; : ' . mysql_error());
} else {
	echo 'Connexion réussie à la base MySql :'.$connex.'<br>';
}
mysql_set_charset('utf8',$connex);

// Connexion à la base de donnée
$db = mysql_select_db($database, $connex);
if (!$db) {
	die ('Impossible d\'utiliser la base : ' . mysql_error());
} else {
	echo 'Connexion réussie à la base '.$database.'<br>';
}

// Lecture de la table contact
$sql = "SELECT * FROM `contact`";
$query = mysql_query($sql) or die("Erreur: " . mysql_error());
echo 'mysql_num_rows = '.mysql_num_rows($query).'<br>';
echo '<br>';

while ($data = mysql_fetch_array($query)) {
	extract($data);
	echo 'La base de donnée contient :'."\n";
	echo $con_id."\n";
	echo $con_nom."\n";
	echo $con_email."\n";
	echo $con_message."\n";
	echo "\n";
}

?>
</pre>
</body>
</html>