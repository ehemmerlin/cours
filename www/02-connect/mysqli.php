<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Cours 02 Connect</title>
</head>
<body>
<pre>
<?php 
/*
  -- Structure de la table
  CREATE TABLE IF NOT EXISTS `contact` (
  `con_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `con_nom` varchar(255) NOT NULL,
  `con_email` varchar(255) NOT NULL,
  `con_message` text NOT NULL,
  PRIMARY KEY (`con_id`)
  ) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

  -- Contenu de la table
  INSERT INTO `contact` (`con_id`, `con_nom`, `con_email`, `con_message`) VALUES
  (1, 'Pierre Martin', 'pm@gmail.com', 'Super site');
 */

$host			= 'localhost';		// NOM DU SERVEUR SQL
$user			= 'root';			// VOTRE NOM D'UTILISATEUR SQL
$pass			= '';				// VOTRE MOT DE PASSE SQL
$database       = 'cv';		// NOM DE VOTRE BASE DE DONNEES

// Connexion à MySql
$connex = mysqli_connect($host , $user, $pass, $database);
if (!$connex) {
	die('Non connect&eacute; : ' . mysql_error());
}

mysqli_set_charset($connex,'utf8');

// Lecture de la table contact
$sql = "SELECT * FROM `contact`";
$query = mysqli_query($connex, $sql) or die("Erreur: " . mysql_error());
echo 'mysql_num_rows = '.mysqli_num_rows($query).'<br>';
echo '<br>';

while ($data = mysqli_fetch_array($query)) {
	extract($data);
	echo 'La base de donnée contient :'."\n";
	echo $con_id."\n";
	echo $con_nom."\n";
	echo $con_email."\n";
	echo $con_message."\n";
	echo "\n";
}

mysqli_free_result($query);

mysqli_close($connex);

?>
</pre>
<pre>
<?php
// Connexion à MySql
$connex = mysqli_connect($host , $user, $pass, $database);
if (mysqli_connect_errno($connex)) {
	die('Non connect&eacute; : ' . mysqli_connect_error());
}

mysqli_set_charset($connex,'utf8');

// Lecture de la table contact
$sql = "SELECT * FROM `contact`";
$query = mysqli_query($connex, $sql) or die("Erreur: " . mysql_error());
echo 'mysql_num_rows = '.mysqli_num_rows($query).'<br>';
echo '<br>';

while ($data = mysqli_fetch_array($query)) {
	extract($data);
	echo 'La base de donnée contient :'."\n";
	echo $con_id."\n";
	echo $con_nom."\n";
	echo $con_email."\n";
	echo $con_message."\n";
	echo "\n";
}

mysqli_free_result($query);

mysqli_close($connex);

?>
</pre>
</body>
</html>