<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Cours 02 Connect</title>
</head>
<body>
<pre>
<?php 
/*
  -- Structure de la table
  CREATE TABLE IF NOT EXISTS `contact` (
  `con_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `con_nom` varchar(255) NOT NULL,
  `con_email` varchar(255) NOT NULL,
  `con_message` text NOT NULL,
  PRIMARY KEY (`con_id`)
  ) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

  -- Contenu de la table
  INSERT INTO `contact` (`con_id`, `con_nom`, `con_email`, `con_message`) VALUES
  (1, 'Pierre Martin', 'pm@gmail.com', 'Super site');
 */

$host			= 'localhost';		// NOM DU SERVEUR SQL
$user			= 'root';			// VOTRE NOM D'UTILISATEUR SQL
$pass			= '';				// VOTRE MOT DE PASSE SQL
$database       = 'cv';		// NOM DE VOTRE BASE DE DONNEES

$dsn = 'mysql:host='.$host.';dbname='.$database;

try {
	$pdo_options[PDO::ATTR_ERRMODE] = PDO::ERRMODE_EXCEPTION;
	$pdo_options[PDO::MYSQL_ATTR_INIT_COMMAND] = "SET NAMES utf8";
	$bdd = new PDO($dsn, $user, $pass, $pdo_options);
} catch (PDOException $e) {
	die('Erreur : ' . $e->getMessage());
}

try {

	$sql = "SELECT * FROM `contact`";
	$query = $bdd->query($sql);
	echo 'rowCount = '.$query->rowCount().'<br>';
	while($donnees = $query->fetch()) {
		extract($donnees);
		echo 'La base de donnée contient :'."\n";
		echo $con_id."\n";
		echo $con_nom."\n";
		echo $con_email."\n";
		echo $con_message."\n";
		echo "\n";
	}
	$query->closeCursor();
	
	$bdd=null;
	
} catch (PDOException $e) {
	die('Erreur : ' . $e->getMessage());
}

?>
</pre>
</body>
</html>