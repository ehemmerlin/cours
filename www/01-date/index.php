<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Cours 01 Date</title>
</head>
<body>
<p>
<?php
// Affiche le jour en français
setlocale(LC_TIME, "C");
echo strftime("%A");
setlocale(LC_TIME, "french");
echo strftime(" in French is %A.").'<br>';
?>
</p><p>
<?php
// Affiche différents timestamp
echo "Aujourd'hui, le timestamp est : ".time().'<br>';
echo "Hier, le timestamp était : ".(time()-24*3600).'<br>';
echo "Le premier jour de la formation PHP le timestamp était : ".mktime(9,0,0,4,23,2012).'<br>';
?>
</p><p>
<?php 
// Vérifie si le 23/4/2012 existe et affiche le jour
if (checkdate(4,23,2012)) {
	echo strftime("%A",strtotime("4/23/2012")).'<br>'; // Affiche lundi
	echo date("l",mktime(9,0,0,4,23,2012)).'<br>'; // Affiche Monday
}
?>
</body>
</html>