<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Cours 01 Function</title>
</head>

<body>
	<p>
		<?php
		// Appel de fonction externe
		require("functions.php");
		echo myFunction("Bonjour", 'Jeff');
		?>
		<br>
		<?php
		// Définition et appel de fonction
		function dire_texte($qui, $texte = 'Bonjour') {
			if(empty($qui)){
				// Si $qui est vide, on retourne faux
				return FALSE;
			}else{
				echo "$texte $qui"; // On affiche le texte
				return TRUE; // Fonction exécutée avec succès
			}
		}
		if (!dire_texte('')) {
			echo 'Erreur';
		}
		if (!dire_texte('Jeff')) {
			echo 'Erreur';
		}
		if(!dire_texte('Jeff','Bonsoir')) {
			echo 'Erreur';
		}
		?>
		<br>
		<?php
		// Définition et appel de fonction retournant plusieurs valeurs
		function traduction() {
			$tab[] = 'nom' ;
			$tab[] = 'prenom' ;
			$tab[] = 'telephone' ;
			return $tab;
		}
		$ret_traduction = traduction();
		print_r($ret_traduction);
		// Le mot-clé list() permet d’affecter en une opération plusieurs
		// variables venant d’un tableau.
		list($nom, $prenom, $telephone) = traduction() ;
		echo "$nom $prenom, $telephone" ; // Affiche nom prenom, telephone
		?>
		<br>
		<?php
		//Portée de variables
		$chaine = 'Nombre de camions : ';
		function ajoute_camion($mode='') {
			// Le niveau global permet à une variable d’être visible dans la
			// fonction et à l’extérieur de la fonction
			global $chaine;
			// Le niveau static permet de disposer dans une fonction d’une
			// variable locale persistant durant toute l’exécution du script.
			static $nb=0;
			$nb++;
			// On incrémente le nombre de camions
			if($mode == 'affiche') {
				//Le niveau local, utilisé par défaut, permet de définir une
				// variable qui ne sera visible que dans la fonction en cours
				$texte = $chaine.$nb;
				echo $texte;
				// On affiche le nombre de camions
			}
		}
		ajoute_camion(); // nb == 1
		ajoute_camion(); // nb == 2
		ajoute_camion(); // nb == 3
		ajoute_camion('affiche');
		// Affiche Nombre de camions : 4
		?>
		<br>
		<?php
		//Nombre de paramètres indéfinis
		function indefinie() {
			// func_num_args(): le nombre d’arguments utilisés pour appeler la fonction
			// func_get_arg(): récupérer un argument à partir de sa position
			// func_get_args(): retourne un tableau contenant les arguments utilisés
			//  à l’appel de la fonction.
			echo 'Il y a eu ', func_num_args(), ' arguments : ' ;
			$tab = func_get_args();
			// implode() rassemble les éléments d'un tableau en une chaîne
			echo implode(', ', $tab) ;
		}
		indefinie(0, 1, 2, 3 ,4, 5, 6) ;
		// Affiche : Il y a eu 7 arguments : 0, 1, 2, 3 ,4, 5, 6
		?>
	</p>
</body>
</html>
