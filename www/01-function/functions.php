<?php
/**
 * Affiche un message d'accueil
 * @param string $message Message d'accueil
 * @param string $nom Nom à afficher
 */
function myFunction($message, $nom) {
	return "$message $nom";
}

?>