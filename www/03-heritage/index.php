<?php 
require_once "member.php";
require_once "administrator.php";
?>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
</head>
<body>
<h1>Etape 1</h1>
<?php 
// Création d'unn membre et login
$member = new Member();
$member->username = "Fred";
$member->login();
echo $member->username . " est " . ( $member->isLoggedIn() ? "connecté" : "déconnecté" ) . "<br>";
 
// Créatino d'un administrateur et login
$admin = new Administrator();
$admin->username = "Mary";
$admin->login();
echo $admin->username . " est " . ( $member->isLoggedIn() ? "connecté" : "déconnecté" ) . "<br>";
 
// Affiche "Mary a créé un nouveau forum : Teddy Bears"
$admin->createForum( "Teddy Bears" );
 
// Affiche "Mary a bani le membre : Fred"
$admin->banMember( $member );?>
<h1>Etape 2</h1>
<?php 
?>
</body>
</html>