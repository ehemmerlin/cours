<?php
/**
 * Classe Administrator
 * @author HB
 *
 */
class Administrator extends Member {
 
  public function createForum( $forumName ) {
    echo "$this->username a créé un nouveau forum: $forumName<br>";
  }
 
  public function banMember( $member ) {
    echo "$this->username a banni le membre: $member->username<br>";
  }
 
}
?>