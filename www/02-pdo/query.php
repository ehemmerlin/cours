<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Cours 02 PDO</title>
</head>
<body>
<pre>
<?php 
/*
  -- Structure de la table
  CREATE TABLE IF NOT EXISTS `contact` (
  `con_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `con_nom` varchar(255) NOT NULL,
  `con_email` varchar(255) NOT NULL,
  `con_message` text NOT NULL,
  PRIMARY KEY (`con_id`)
  ) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

  -- Contenu de la table
  INSERT INTO `contact` (`con_id`, `con_nom`, `con_email`, `con_message`) VALUES
  (1, 'Pierre Martin', 'pm@gmail.com', 'Super site');
 */

// Déclaration des variables de connexion
$host			= 'localhost';		// NOM DU SERVEUR SQL
$user			= 'root';			// VOTRE NOM D'UTILISATEUR SQL
$pass			= '';				// VOTRE MOT DE PASSE SQL
$database       = 'cv';		// NOM DE VOTRE BASE DE DONNEES

$dsn = 'mysql:host='.$host.';dbname='.$database;

?> 
</pre>
Etape 1 : Connexion à la base de donnée
<pre>
<?php

// Connexion à la base de donnée
try {
	$pdo_options[PDO::ATTR_ERRMODE] = PDO::ERRMODE_EXCEPTION;
	$pdo_options[PDO::MYSQL_ATTR_INIT_COMMAND] = "SET NAMES utf8";
	$dbh = new PDO($dsn, $user, $pass, $pdo_options);
	echo 'Connexion réussie';
} catch (PDOException $e) {
	die('Erreur : ' . $e->getMessage());
}

?>
</pre>
Etape 2 : Sélection des enregistrements avec différents types de retour
<pre>
<?php

// Sélection des enregistrements avec différents types de retour
try {

	$sql = "SELECT * FROM `contact`";
	// Lecture d’enregistrements
	$sth = $dbh->query($sql);
	$result = $sth->fetchAll(PDO::FETCH_ASSOC);
	echo 'FETCH_ASSOC'."\n";
	print_r($result);
	$sth = $dbh->query($sql);
	$result = $sth->fetchAll(PDO::FETCH_BOTH);
	echo 'FETCH_BOTH'."\n";
	print_r($result);
	$sth = $dbh->query($sql);
	$result = $sth->fetchAll(PDO::FETCH_OBJ);
	echo 'FETCH_OBJ'."\n";
	print_r ($result);
	
	$sth->closeCursor();
		
} catch (PDOException $e) {
	die('Erreur : ' . $e->getMessage());
}

?>
</pre>
Etape 3 : Sélection et extraction des données avec fetchAll()
<pre>
<?php

// Sélection et extraction des données avec fetchAll()
try {

	$sql = "SELECT * FROM `contact`";
	$sth = $dbh->query($sql);
	$result = $sth->fetchAll(PDO::FETCH_ASSOC);
	foreach ($result as $row){
		echo $row['con_nom']; echo '-';
		echo $row['con_email']; echo '-';
		echo $row['con_message']; echo '<br/>';
	}
	
	$nombre = count($result);
	echo 'Nombre d\'enregistrements retournés '.$nombre."\n";
	
	$sth->closeCursor();

} catch (PDOException $e) {
	die('Erreur : ' . $e->getMessage());
}

?>
</pre>
Etape 4 : Sélection et extraction des données avec fetch()
<pre>
<?php

// Sélection et extraction des données avec fetch()
try {

	$sql = "SELECT * FROM contact";
	$sth = $dbh->query($sql);
	while($row = $sth->fetch(PDO::FETCH_ASSOC)){
		print_r($row);
	}

	$sql = "SELECT count(*) as nbe FROM contact";
	$sth = $dbh->query($sql);
	$result = $sth->fetchAll();
	$nombre = $result[0]['nbe'];
	echo 'Nombre d\'enregistrements retournés '.$nombre."\n";

	$sth->closeCursor();

} catch (PDOException $e) {
	die('Erreur : ' . $e->getMessage());
}

?>
</pre>
Etape 5 : Fermeture de la connexion
<pre>
<?php

// Fermeture de la connexion
if ($dbh) {
	$dbh = NULL;
}

?>
</pre>
</body>
</html>