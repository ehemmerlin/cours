<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Cours 02 PDO</title>
</head>
<body>
<pre>
<?php 
/*
  -- Structure de la table
  CREATE TABLE IF NOT EXISTS `contact` (
  `con_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `con_nom` varchar(255) NOT NULL,
  `con_email` varchar(255) NOT NULL,
  `con_message` text NOT NULL,
  PRIMARY KEY (`con_id`)
  ) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

  -- Contenu de la table
  INSERT INTO `contact` (`con_id`, `con_nom`, `con_email`, `con_message`) VALUES
  (1, 'Pierre Martin', 'pm@gmail.com', 'Super site');
 */

// Déclaration des variables de connexion
$host			= 'localhost';		// NOM DU SERVEUR SQL
$user			= 'root';			// VOTRE NOM D'UTILISATEUR SQL
$pass			= '';				// VOTRE MOT DE PASSE SQL
$database       = 'cv';		// NOM DE VOTRE BASE DE DONNEES

$dsn = 'mysql:host='.$host.';dbname='.$database;

?> 
</pre>
Etape 1 : Connexion à la base de donnée
<pre>
<?php

// Connexion à la base de donnée
try {
	$pdo_options[PDO::ATTR_ERRMODE] = PDO::ERRMODE_EXCEPTION;
	$pdo_options[PDO::MYSQL_ATTR_INIT_COMMAND] = "SET NAMES utf8";
	$dbh = new PDO($dsn, $user, $pass, $pdo_options);
	echo 'Connexion réussie';
} catch (PDOException $e) {
	die('Erreur : ' . $e->getMessage());
}

?>
</pre>
Etape 2 : Transaction
	<pre>
<?php

// Démarre une transaction, désactivation de l'auto-commit
$dbh->beginTransaction();
try {
	// Ajout d’un enregistrement
	$sql = "INSERT INTO contact (con_nom, con_email, con_message)
	VALUES ('Rafael Nadal','rnadal@gmail.com', 'Tu peux mettre mon CV sur ton site ? Merci')";
	$dbh->exec($sql);
	// Ajout d’un second enregistrement dépendant du premier
	// syntaxe incorrecte dans cet exemple : tit1re
	$sql2 = "INSERT INTO contact (con_nom, con_email, con_message)
	VALUES ('Roger Federer','rfederer@gmail.com', 'Tu peux aussi mettre mon CV sur ton site. Merci')";
	$dbh->exec($sql2);
	// Si les requêtes se sont bien passées, on valide
	// Sinon une exception a été lancée
	$dbh->commit();
	echo 'Transaction réussie';
} catch (Exception $e){
	// S’il y a eu une erreur, on annule les modifications
	$dbh->rollBack();
	echo "Echec - Transaction annulée : " . $e->getMessage();
}
?>
</pre>
	Etape 5 : Fermeture de la connexion
<pre>
<?php

// Fermeture de la connexion
if ($dbh) {
	$dbh = NULL;
}

?>
</pre>
</body>
</html>