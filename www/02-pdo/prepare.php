<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Cours 02 PDO</title>
</head>
<body>
<pre>
<?php 
/*
  -- Structure de la table
  CREATE TABLE IF NOT EXISTS `contact` (
  `con_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `con_nom` varchar(255) NOT NULL,
  `con_email` varchar(255) NOT NULL,
  `con_message` text NOT NULL,
  PRIMARY KEY (`con_id`)
  ) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

  -- Contenu de la table
  INSERT INTO `contact` (`con_id`, `con_nom`, `con_email`, `con_message`) VALUES
  (1, 'Pierre Martin', 'pm@gmail.com', 'Super site');
 */

// Déclaration des variables de connexion
$host			= 'localhost';		// NOM DU SERVEUR SQL
$user			= 'root';			// VOTRE NOM D'UTILISATEUR SQL
$pass			= '';				// VOTRE MOT DE PASSE SQL
$database       = 'cv';		// NOM DE VOTRE BASE DE DONNEES

$dsn = 'mysql:host='.$host.';dbname='.$database;

?> 
</pre>
Etape 1 : Connexion à la base de donnée
<pre>
<?php

// Connexion à la base de donnée
try {
	$pdo_options[PDO::ATTR_ERRMODE] = PDO::ERRMODE_EXCEPTION;
	$pdo_options[PDO::MYSQL_ATTR_INIT_COMMAND] = "SET NAMES utf8";
	$dbh = new PDO($dsn, $user, $pass, $pdo_options);
	echo 'Connexion réussie';
} catch (PDOException $e) {
	die('Erreur : ' . $e->getMessage());
}

?>
</pre>
Etape 2 : Insertion d'un enregistrement préparé
<pre>
<?php

// Insertion d'un enregistrement préparé
try {

	// Insertion d’un enregistrement
	$sql = "INSERT INTO contact (con_nom, con_email, con_message)
	VALUES (:nom, :email, :message)";
	$stmt = $dbh->prepare($sql);
	$nom = "Robert d'Aler";
	$email = "rdaler@gmail.com";
	$message = "Merci. Signé Robert d'Aler.";
	$stmt->execute(array(':nom'=>$nom, ':email'=>$email, ':message'=>$message));

	echo 'lastInsertId = '.$dbh->lastInsertId();
	
} catch (PDOException $e) {
	die('Erreur : ' . $e->getMessage());
}

?>
</pre>
Etape 2 : Insertion d'un enregistrement préparé avec bind
<pre>
<?php

// Insertion d'un enregistrement préparé avec bind
try {

	// Insertion d’un enregistrement
	$sql = "INSERT INTO contact (con_nom, con_email, con_message)
	VALUES (:nom, :email, :message)";
	$stmt = $dbh->prepare($sql);
	$nom = "Robert d'Aler";
	$email = "rdaler@gmail.com";
	$message = "Merci. Signé Robert d'Aler.";
	$stmt->BindParam(':nom',$nom);
	$stmt->BindParam(':email',$email);
	$stmt->BindParam(':message',$message);
	$stmt->execute();

	$nom = 'Anonyme';
	$stmt->execute(); // Ne marche que parcequ'on a utilisé BinParam et non BindValue
	
	echo 'lastInsertId = '.$dbh->lastInsertId();
		
} catch (PDOException $e) {
	die('Erreur : ' . $e->getMessage());
}

?>
</pre>
Etape 3 : Affichage des données préparées avec fectchAll()
<pre>
<?php

// Affichage des données préparées avec fetchAll()
try {

	$nom = 'Bill Gates';
	$sql = "SELECT * FROM `contact` WHERE con_nom<>:nom";
	$stmt = $dbh->prepare($sql);
	$stmt->execute(array(':nom'=>$nom));
	$result = $stmt->fetchAll(PDO::FETCH_ASSOC);
	foreach ($result as $row){
		echo $row['con_nom']; echo '-';
		echo $row['con_email']; echo '-';
		echo $row['con_message']; echo '<br/>';
	}
	
	$nombre = count($result);
	echo 'Nombre d\'enregistrements retournés '.$nombre."\n";
	
	$stmt = NULL;

} catch (PDOException $e) {
	die('Erreur : ' . $e->getMessage());
}

?>
</pre>
Etape 4 : Suppression des données
<pre>
<?php

// Suppression des données
try {

	$sql = "DELETE FROM contact WHERE con_email IN ('rnadal@gmail.com','rdaler@gmail.com','rfederer@gmail.com')";
	// Modification d’enregistrement
	$retour = $dbh->exec($sql);
	if($retour === FALSE){
		die('Erreur dans la requête') ;
	}elseif($retour === 0){
		echo 'Aucune modification effectuée';
	}else{
		echo $retour . ' ligne(s) a(ont) été(s) affectée(s).';
	}

} catch (PDOException $e) {
	die('Erreur : ' . $e->getMessage());
}

?>
</pre>
Etape 5 : Affichage des données préparées avec fetch()
<pre>
<?php

// Affichage des données préparées avec fetch()
try {

	$nom = 'Bill Gates';
	$sql = "SELECT * FROM `contact` WHERE con_nom<>:nom";
	$stmt = $dbh->prepare($sql);
	$stmt->execute(array(':nom'=>$nom));
	while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
		echo $row['con_nom']; echo '-';
		echo $row['con_email']; echo '-';
		echo $row['con_message']; echo '<br/>';
	}

	$nombre = count($result);
	echo 'Nombre d\'enregistrements retournés '.$nombre."\n";

	$stmt = NULL;

} catch (PDOException $e) {
	die('Erreur : ' . $e->getMessage());
}


?>
</pre>
Etape 5 : Fermeture de la connexion
<pre>
<?php

// Fermeture de la connexion
if ($dbh) {
	$dbh = NULL;
}

?>
</pre>
</body>
</html>