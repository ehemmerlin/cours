<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Cours 01 Condition</title>
</head>
<body>
	<p>
		<?php
		// Structure de contrôle If avec condition simple
		$age = 25;
		if($age < 18) {
			echo 'Vous êtes trop jeune pour entrer ici';
			exit(); // La fonction exit() arrête l’exécution du script
		}
		?>
		<br>
		<?php
		// Structure de contrôle If avec condition complexe
		$age = 25;
		if( ($age > 18) && ($age < 35) ) {
			echo 'Votre profil est : jeune adulte';
		}
		?>
		<br>
		<?php
		// Structure de contrôle If Else
		$temps = 'moche';
		if($temps == 'ensoleillé') {
			echo 'Il fait beau';
		}else{
			echo 'Il ne fait pas beau';
		}
		?>
		<br>
		<?php
		$nombre = 2 ;
		if ($nombre > 1) {
			echo "$nombre est supérieur à 1";
		} elseif ($nombre < 1) {
			echo "$nombre est inférieur à 1";
		} else {
			echo "$nombre est égal à 1";
			// Ici on sait que $nombre est égal à 1 car s’il n’est
			// ni supérieur, ni inférieur à 1 c’est qu’il est égal à 1.
		}
		?>
		<br>
		<?php
		// Attention aux accolades
		if($temps == 'ensoleillé')
			echo 'Il fait beau';
		echo ' et chaud'; // Cette instruction sera toujours réalisée
		?>
		<br>
		<?php
		// Structure de contrôle Switch
		$nombre = mt_rand(0,4); 
		// La fonction prend en argument deux paramètres, le minimum et le maximum,
		// et fournit une valeur aléatoire comprise entre ces deux valeurs.
		switch ($nombre) {
			case 4:
				echo "$nombre est supérieur à 3 <br>";
			case 3:
				echo "$nombre est supérieur à 2 <br>";
			case 2:
				echo "$nombre est supérieur à 1 <br>";
			case 1:
				echo "$nombre est supérieur à 0 <br>";
				break ;
			default:
				echo "$nombre est 0 <br>";
		}
		?>
		<br>
	</p>
</body>
</html>
