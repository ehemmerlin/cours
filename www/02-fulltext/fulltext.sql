-- Recherche FULLTEXT
-- Introdution aux outils suivants :
--   FULLTEXT, MATCH


-- Création de la base de donnée cours
CREATE DATABASE IF NOT EXISTS cours;

-- Placement sur la base de donnée cours
USE cours;

-- Suppression de la table articles
DROP TABLE IF EXISTS articles;

-- Création de la table articles
CREATE TABLE articles (
    id INT UNSIGNED AUTO_INCREMENT NOT NULL PRIMARY KEY,
    title VARCHAR(200),
    body TEXT,
    FULLTEXT ( title , body )
)  ENGINE=MyISAM;


INSERT INTO articles VALUES
     (NULL,'MySQL Tutorial', 'DBMS stands for DataBase ...'),
     (NULL,'How To Use MySQL Efficiently', 'After you went through a ...'),
     (NULL,'Optimising MySQL','In this tutorial we will show ...'),
     (NULL,'1001 MySQL Tricks','1. Never run mysqld as root. 2. ...'),
     (NULL,'MySQL vs. YourSQL', 'In the following database comparison ...'),
     (NULL,'MySQL Security', 'When configured properly, MySQL ...');

-- Recherche FULLTEXT de database
SELECT 
    *
FROM
    articles
WHERE
    MATCH (title , body) AGAINST ('database' );
    
-- Recherche FULLTEXT de database par MATCH
SELECT 
    id, MATCH (title , body) AGAINST ('database' )
FROM
    articles;
    
-- Recherche FULLTEXT de MySQL
SELECT 
    *
FROM
    articles
WHERE
    MATCH (title , body) AGAINST ('MySQL' );