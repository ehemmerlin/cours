-- Sélection d'enregistrements
-- Introdution aux outils suivants :
--   BINARY, AND, OR, IN, BETWEEN, MIN, LIKE, <, >
--   DISTINCT, LIMIT, ORDER BY, COUNT


-- Création de la base de donnée cours
CREATE DATABASE IF NOT EXISTS cours;

-- Placement sur la base de donnée cours
USE cours;

-- Suppression de la table client
DROP TABLE IF EXISTS client;

-- Création de la table client
CREATE TABLE IF NOT EXISTS client (
    cli_id MEDIUMINT UNSIGNED NOT NULL AUTO_INCREMENT,
    cli_nom VARCHAR(30) NOT NULL,
    cli_prenom VARCHAR(30) NOT NULL,
    cli_categorie VARCHAR(40) collate utf8_general_ci NULL DEFAULT NULL,
    cli_adresse VARCHAR(60) NOT NULL,
    cli_mail VARCHAR(50) NULL DEFAULT 'pas de mail',
    cli_sexe CHAR(1) NULL,
    cli_date DATE NULL DEFAULT NULL,
    PRIMARY KEY (cli_id)
)  ENGINE=InnoDB DEFAULT CHARACTER SET=utf8 COLLATE = utf8_general_ci; -- ENGINE=InnoDB -- ENGINE=MEMORY -- ENGINE=MyISAM

-- Insertion de données dans la table client
INSERT INTO `cours`.`client` (`cli_nom`, `cli_prenom`, `cli_adresse`,`cli_categorie`, `cli_mail`, `cli_sexe`) VALUES ('Nadal', 'Rafael', '21 rue du Grand Chelem 75000 PARIS', 'tennis', 'r.nadal@gmail.com', 'M');
INSERT INTO `cours`.`client` (`cli_nom`, `cli_prenom`, `cli_adresse`,`cli_categorie`, `cli_sexe`) VALUES ('Bolelli', 'Simone', '5 rue de la Raquette 75000 PARIS', 'Tennis', 'M');
INSERT INTO `cours`.`client` (`cli_nom`, `cli_prenom`, `cli_adresse`,`cli_categorie`,`cli_date`, `cli_sexe`) VALUES ('Douillet', 'David', '5 rue du Poids Lourd 75000 PARIS','Judo','1969/02/17', 'M');
INSERT INTO `cours`.`client` (`cli_nom`, `cli_prenom`, `cli_adresse`,`cli_categorie`,`cli_date`) VALUES ('Makarov', 'Vitali', '5 rue du Poids Léger 75000 PARIS','judo','19740623');

-- Affichage des données insérées
SELECT 
    *
FROM
    client ; -- ORDER BY cli_categorie;
    
-- BINARY
SELECT * FROM client WHERE cli_categorie = BINARY 'Tennis' LIMIT 2 ; -- BINARY
SELECT * FROM client WHERE cli_categorie = 'Tennis' LIMIT 2 ; -- BINARY

-- AND
SELECT * FROM client WHERE cli_categorie = 'Tennis' AND cli_nom = 'Nadal' ;

-- OR
SELECT * FROM client WHERE cli_categorie = 'Tennis' OR cli_nom = 'Douillet' ;

-- IN
SELECT * FROM client WHERE cli_categorie IN ('rugby', 'tennis', 'foot') ; -- BINARY

-- BETWEEN
SELECT * FROM client WHERE cli_date BETWEEN '1970/01/01' AND '1980/01/01';

-- LIKE
SELECT * FROM client WHERE cli_prenom LIKE 'Vit%' AND cli_nom LIKE '%ov';

-- > <
SELECT * FROM client WHERE cli_date > '1970/01/01';
SELECT * FROM client WHERE cli_date < '1970/01/01';

-- MIN
SELECT MIN(cli_date) AS minimum FROM client;

-- COUNT
SELECT COUNT(*) AS nb_result FROM client WHERE cli_categorie='judo';

-- ORDER BY
SELECT * FROM client ORDER BY cli_nom DESC; -- ASC

-- LIMIT
SELECT * FROM client LIMIT 2,2; -- Retourne les enregistrements 3 à 4 avec MySQL

-- DISTINCT
SELECT DISTINCT cli_categorie FROM client;