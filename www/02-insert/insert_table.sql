-- Insertion de données dans une table
-- Introdution aux outils suivants :
--   INSERT


-- Création de la base de donnée cours
CREATE DATABASE IF NOT EXISTS cours;

-- Placement sur la base de donnée cours
USE cours;

-- Suppression de la table client
DROP TABLE IF EXISTS client;

-- Création de la table client
CREATE TABLE IF NOT EXISTS client (
    cli_id MEDIUMINT UNSIGNED NOT NULL AUTO_INCREMENT,
    cli_nom VARCHAR(30) NOT NULL,
    cli_prenom VARCHAR(30) NOT NULL,
    cli_categorie VARCHAR(40) collate utf8_general_ci NULL DEFAULT NULL,
    cli_adresse VARCHAR(60) NOT NULL,
    cli_mail VARCHAR(50) NULL DEFAULT 'pas de mail',
    cli_sexe CHAR(1) NULL,
    cli_date DATE NULL DEFAULT NULL,
    PRIMARY KEY (cli_id)
)  ENGINE=InnoDB DEFAULT CHARACTER SET=utf8 COLLATE = utf8_general_ci; -- ENGINE=InnoDB -- ENGINE=MEMORY -- ENGINE=MyISAM

-- Insertion de données sans préciser le nom des colonnes
INSERT INTO client VALUES (1,'Nadal', 'Rafael', 'tennis', '21 rue du Grand Chelem 75000 PARIS', 'r.nadal@gmail.com', 'M', NULL);

-- Insertion de données multiples
INSERT INTO `cours`.`client` (`cli_nom`, `cli_prenom`, `cli_adresse`,`cli_categorie`, `cli_sexe`) 
VALUES ('Bolelli', 'Simone', '5 rue de la Raquette 75000 PARIS', 'Tennis', 'M'),
('Douillet', 'David', '5 rue du Poids Lourd 75000 PARIS','Judo', 'M'),
('Makarov', 'Vitali', '5 rue du Poids Léger 75000 PARIS','judo','M');

-- Affichage des données insérées
SELECT 
    *
FROM
    client ; -- ORDER BY cli_categorie;
