<?php
/**
 * Classe Member
 * @author HB
 *
 */
class Member
{
	/**
	 * Attribut nom du membre
	 * @var string
	 */
	public $username = "";

	/**
	 * Attribut de localisation
	 * @var string
	 */
	private $location;
	/**
	 * Attribut de page internet
	 * @var string
	 */
	private $homepage;
	
	/**
	 * Constructeur
	 * @param unknown_type $username
	 * @param unknown_type $location
	 * @param unknown_type $homepage
	 */
	public function __construct( $username, $location, $homepage ) {
		$this->username = $username;
		$this->location = $location;
		$this->homepage = $homepage;
	}
	 
	/**
	 * Destructeur
	 */
	public function __destruct() {
		echo "Je vais disparaître";
	}
	
	/**
	 * Affiche le profile
	 */
	public function showProfile() {
		echo "<dl>";
		echo "<dt>Username:</dt><dd>$this->username</dd>";
		echo "<dt>Location:</dt><dd>$this->location</dd>";
		echo "<dt>Homepage:</dt><dd>$this->homepage</dd>";
		echo "</dl>";
	}
	
	/**
	 * Attribut indiquant si le membre est loggé
	 * @var bool
	 */
	private $loggedIn = false;

	/**
	 * Méthode loggin()
	 */
	public function login() {
		$this->loggedIn = true;
	}
	
	/**
	 * Méthode logout
	 */
	public function logout() {
		$this->loggedIn = false;
	}
	
	/**
	 * Méthode isLoggedIn
	 */
	public function isLoggedIn() {
		return $this->loggedIn;
	}
}
?>