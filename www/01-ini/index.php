<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Cours 01 Ini</title>
</head>
<body>
	<?php
	echo 'La valeur de session.gc_maxlifetime est : ' ,
	ini_get('session.gc_maxlifetime').'<br>';
	
	ini_set('session.gc_maxlifetime',43200);
	
	echo 'La valeur de session.gc_maxlifetime est : ' ,
	ini_get('session.gc_maxlifetime').'<br>';

	ini_restore('session.gc_maxlifetime');
	
	echo 'La valeur de session.gc_maxlifetime est : ' ,
	ini_get('session.gc_maxlifetime').'<br>';
	?>
</body>
</html>
