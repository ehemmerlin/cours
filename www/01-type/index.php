<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Cours 01 Type</title>
</head>
<body>
	<p>
		<?php
		// Vous pouvez connaître le type d’une donnée grâce à la fonction gettype().
		echo gettype("chaîne de caractères") ; // Affiche string
		?>
		<br>
		<?php
		// Il est aussi possible d’utiliser des fonctions d’accès rapide telle que is_string(),
		// qui renvoie TRUE si la valeur en argument est une chaîne de caractères et FALSE dans
		// le cas contraire.
		$var = 12;
		if (is_string($var)) {
			echo "chaîne de caractères" ;
		} else {
			echo "autre type" ; // Affiche autre type
		}
		?>
		<br>
		<?php
		// Forcer une conversion
		echo gettype( "12" ); // Affiche string
		echo gettype( (integer) "12" ); // Affiche integer
		echo gettype( (string) 12 ); // Affiche string
		?>
		<br>
		<?php
		/* Si cette syntaxe vous paraît peu claire, vous pouvez utiliser la
		fonction settype(). Elle prend en premier argument la donnée à
		convertir et en second argument le type vers lequel faire la
		conversion. */
		$var_type="12";
		echo gettype( $var_type ) ; // Affiche string
		settype($var_type,'integer');
		echo gettype( $var_type ) ; // Affiche integer
		settype($var_type, 'string');
		echo gettype( $var_type ) ; // Affiche string
		?>
	</p>
	<pre><?php

		$a = 0;
		$b = FALSE;

		if($a == $b)
		{
			echo "Vrai : a==b.\n";
		} else { echo "Faux : a==b.\n";
		}

		if($a === $b)
		{
			echo "Vrai : a===b.\n";
		} else { echo "Faux : a===b.\n";
		}
		echo 'a est de type : '.gettype($a)."\n";
		echo 'b est de type : '.gettype($b)."\n";

		exit();
		?>
	</pre>
</body>
</html>
