<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Cours 01 Upload</title>
</head>
<body>
	<?php 
	if (isset($_POST['envoyer'])) {
		// Affichage d'informations sur le fichier
		$fichier = $_FILES['fichier']['name'];
		$taille = $_FILES['fichier']['size'];
		$tmp = $_FILES['fichier']['tmp_name'];
		$type = $_FILES['fichier']['type'];
		$err = $_FILES['fichier']['error'];
		echo "Nom d'origine => $fichier <br />";
		echo "Taille => $taille <br />";
		echo "Adresse temporaire sur le serveur => $tmp <br />";
		echo "Type de fichier => $type <br />";
		echo "Code erreur => $err. <br />";

		// Tableau des extensions autorisées.
		$extensions = array('.png', '.gif', '.jpg', '.jpeg');
		// Récupèration de la partie de la chaine à partir du dernier . pour connaître l'extension
		$ext = strstr($fichier, '.');
		// Récupèration du nom du fichier
		$filename = strstr($fichier, '.',true);
		// Test si l'extension est dans le tableau
		if(!in_array($ext, $extensions)) //Si l'extension n'est pas dans le tableau
		{
			echo 'Vous devez uploader un fichier de type '.implode(' ',$extensions);
		} else {
			if ($err) {
				echo "il y a eu une erreur<br>" ;
				if ($err == UPLOAD_ERR_INI_SIZE) {
					echo "Le fichier est plus gros que le max autorisé par PHP";
				}
				elseif ($err == UPLOAD_ERR_FORM_SIZE) {
					echo "Le fichier est plus gros qu’indiqué dans le formulaire";
				}
				elseif ($err == UPLOAD_ERR_PARTIAL) {
					echo "Le fichier n'a été que partiellement téléchargé";
				}
				elseif ($err == UPLOAD_ERR_NO_FILE) {
					echo "Aucun fichier n'a été téléchargé.";
				}
			} else {
				// Enregistrement du fichier
				echo "fichier correctement téléchargé" ;
				$nom_destination = $filename.$ext;
				move_uploaded_file($tmp, $nom_destination);
			}
		}
	}
	?>
	<form action="index.php" method="POST" enctype="multipart/form-data">
		<p>
			<input type="file" name="fichier" size="40"> <br>
			<input type="submit" name="envoyer" value="Envoyer">
		</p>
	</form>
</body>
</html>
