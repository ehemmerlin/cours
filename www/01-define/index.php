<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Cours 01 Define</title>
</head>
<body>
	<p>
		<?php
		define("CONSTANT", "Bonjour le monde.");
		echo CONSTANT; // affiche "Bonjour le monde."
		echo Constant; // affiche "Constant" et émet une alerte

		define("GREETING", "Salut toi.<br>", true);
		echo GREETING; // affiche "Salut toi."
		echo Greeting; // affiche "Salut toi."

		?>
	</p>
</body>
</html>
