<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Cours 01 Fichier</title>
</head>
<body>
<p>
<?php 
echo 'Répertoire courant : ' , getcwd().'<br>';
//On descend d’un niveau
chdir ('..');
echo 'Répertoire courant : ' , getcwd().'<br>';
chdir ('01-fichier');
echo 'Répertoire courant : ' , getcwd().'<br>';

// Lecture du contenu d'un répertoire
function lecture_rep() {
	echo '<br><b>Lecture du contenu du répertoire</b><br>';
	$dir = opendir(".") ;
	$fichiers = array() ;
	while( $nom = readdir($dir) ) {
		$fichiers[] = $nom ;
		echo $nom.'<br>';
	}
	closedir($dir) ;
}

// Lecture du contenu d'un répertoire
function lecture_fichiers_texte() {
	echo '<br><b>Lecture des fichiers textes</b><br>';
	$fichiers = glob('./*.txt') ;
	print_r($fichiers);
	echo '<br>';
}

lecture_rep();
lecture_fichiers_texte();

// Création d'un fichier
if(!file_exists("monfich.txt"))
{
	echo '<br><b>Ecriture du fichier</b><br>';
	touch("monfich.txt",time());
}

lecture_rep();
lecture_fichiers_texte();

// Suppression d'un fichier
if(file_exists("monfich.txt"))
{
	echo '<br><b>Effacement du fichier</b><br>';
	unlink("monfich.txt");
}

lecture_rep();
lecture_fichiers_texte();
//exit();
?>
</p><p>
<?php 
// Compteur d'accès au fichier
if(file_exists("compteur.txt"))
{
	if($id_file=fopen("compteur.txt","r"))
	{
		$nb=fread($id_file,10);
		$nb++;
		fclose($id_file);
		$id_file=fopen("compteur.txt","w");
		fwrite($id_file,$nb);
		fclose($id_file);
	}
	else {
		echo "fichier introuvable";
	}
} else {
	$nb=10000;
	$id_file=fopen("compteur.txt","w");
	fwrite($id_file,$nb);
	fclose($id_file);
}

$fichier = 'compteur.txt';
echo $fichier , ' fait ' , filesize($fichier) , ' octets';
?>
</p>
</body>
</html>