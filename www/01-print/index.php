<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Cours 01 Print</title>
</head>
<body>
	<p>
		Etape 1 <br>
		<?php 
		echo( 'Il fait beau. ' ) ;
		echo 'Le soleil brille' ;
		?>
	</p>
	<p>
		Etape 2 <br>
		<?php
		$temps = 'il fait beau';
		echo 'Ce matin '. $temps .'.' ;
		?>
	</p>
	<p>
		Etape 3 <br>
		<?php
		$version = 5 ;
		$miroir = 'fr' ;
		$masque = 'PHP %d est disponible sur http://%s.php.net' ;
		printf($masque, $version, $miroir) ;
		?>
	</p>
	<p>
		Etape 4 <br>
		<?php
		$langue = 'fr'; // $_COOKIE['langue'];
		$fr = '%d est la version de %s' ;
		$en = '%d is %s\’s version' ;
		if ($langue ==='en')
		{
			printf($en, 5, 'PHP') ;
		}else{
			printf($fr, 5, 'PHP') ;
		}
		?>
	</p>
	<p>
		Etape 5 <br>
		<?php
		printf('%s <br>', 'PHP5') ; // Affiche PHP5
		printf('%d <br>', -6) ; // Affiche -6
		printf('%f <br>', -6.343) ; // Affiche -6.343
		printf('%b <br>', 3) ; // Affiche 11 car 1*2 + 1*1
		printf('%o <br>', 15) ; // Affiche 17 car 1*8 + 7*1
		printf('%x %X <br>', 16, 17) ; // Affiche 10 11
		?>
	</p>
	<p>
		Etape 6 <br>
		<?php
		printf('%03d', 1) ; // Affiche 001
		?>
	</p>
	<p>
		Etape 7 <br>
		<?php
		printf('%08.4f', 1.0) ; // Affiche 001.0000
		?>
	</p>
	<p>
		Etape 8 <br>
		<?php
		printf('%s', 'bonjour') ; // Affiche "bonjour" directement
		$texte = sprintf('%s', 'bonjour') ; // Met le texte en variable
		echo $texte ; // Affiche "bonjour" lors de l’appel à echo
		?>
	</p>
	<p>
		Etape 9 <br>
		<?php
		$data = array( 'PHP', 5 ) ;
		vprintf('%s %d', $data) ; // Affiche "PHP 5" directement
		$texte = vsprintf('%s %d', $data) ; // Met le texte en variable
		echo $texte ; // Affiche "PHP 5" lors de l’appel à echo
		?>
	</p>
</body>
</html>
