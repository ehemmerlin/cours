<?php
require_once "connexion.inc.php";

function progess_bar($percent) {
	echo '<div class="progress">
	<div class="bar" style="width: '.$percent.'%;"></div>
	</div>';
}

function query( $sql )
{
	global $bdd;
	$t = microtime( true );
	$q = $bdd->query($sql);
	$rcount = $q->rowCount();
	$f = microtime( true );
	if( $q  )
	{
		$tmp = ($f - $t)*1000;
		printf( "<p>%d données %.02f ms : %s</p>", $rcount, ($f - $t)*1000, $sql );
		progess_bar($tmp/100.0);
		return $q;
	}
}
?>
<!DOCTYPE html>
<html lang="fr">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Performance</title>
<!-- Le styles -->
<link href="bootstrap/css/bootstrap.css" rel="stylesheet">
<link href="bootstrap/css/bootstrap-responsive.css" rel="stylesheet">

</head>

<body>

	<div class="container">
		<div id="experiences" class="section">
			<div class="page-header">
				<h1>
					Performances <small>Mesure de performances PHP MySQL</small>
				</h1>
			</div>
			<h2>Fonctions de dates</h2>
			<?php
			query ("SELECT rc_date AS date FROM cours.recettes_vendeurs;");
			query ("SELECT DATE_FORMAT(rc_date,'%y') AS date FROM cours.recettes_vendeurs;");
			query ("SELECT YEAR(FROM_DAYS(TO_DAYS(NOW()) - TO_DAYS(rc_date))) AS age FROM cours.recettes_vendeurs;"); // SQL_NO_CACHE
			query ("SELECT TIMESTAMPDIFF(YEAR,rc_date,CURRENT_TIMESTAMP) AS ageWalid FROM cours.recettes_vendeurs;");
			query ("SELECT YEAR(FROM_DAYS(DATEDIFF(CURDATE(), rc_date))) AS ageYohan FROM cours.recettes_vendeurs;");
			?>
			<div class="well">Conclusion : La recherche de la date en base de
				donnée est plus longue que le calcul de l'âge, les trois traitements
				de calcul étant d'une durée équivalente.</div>
			<h2>Fonctions de calcul</h2>
			<?php
			query ("SELECT rc_montant AS montant FROM cours.recettes_vendeurs;");
			query ("SELECT AVG(rc_montant) AS moyenne FROM cours.recettes_vendeurs;");
			?>
			<div class="well">Conclusion : La recherche de la moyenne est plus
				rapide que la recherche des montants individuels.</div>
			<h2>Fonctions de recherche</h2>
			<?php
			query ("SELECT * FROM cours.recettes_vendeurs;");
			query ("SELECT CONCAT(vd_id,rc_date,rc_montant) FROM cours.recettes_vendeurs;");
			?>
			<div class="well">Conclusion : L'extraction des enregistrements est plus longue que l'extraction d'un enregistrement concaténant l'ensemble.</div>
			<?php
			/*
			// Insertion des données dans la base
			$max_vendeurs	= 100;
			$max_jours	= 5000;

			$values = array();
			for( $vendeur_id=1; $vendeur_id<=$max_vendeurs; $vendeur_id++ ) {
			$values[] = "($vendeur_id,'vendeur $vendeur_id')";
			}

			query( "INSERT INTO vendeurs (vd_id, vd_name) VALUES ".implode(',',$values) );

			for( $jour=0; $jour<$max_jours; $jour++ ) {
			query( "INSERT INTO recettes_vendeurs (vd_id, rc_date, rc_montant)
					SELECT vd_id, DATE_SUB( now(), INTERVAL $jour DAY ), pow(rand(),4) * 10000
					FROM vendeurs" );
			}
			*/
			?>

		</div>

	</div>
	<!-- /container -->
	<script type="text/javascript"
		src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.js"></script>
	<script type="text/javascript" src="bootstrap/js/bootstrap-alert.js"></script>
	<script type="text/javascript" src="bootstrap/js/bootstrap-dropdown.js"></script>
</body>
</html>

<?php 
/*
 // Exemple
// Mesure des performances d'un code PHP
$start = microtime(true);
	
for($i = 0; $i < 10000; $i++) //loop 10K times
{
//test code here
}
	
$end = microtime(true);
	
$time = $end - $start;
*/
?>

