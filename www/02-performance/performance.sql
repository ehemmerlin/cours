DROP TABLE vendeurs, recettes_vendeurs;
CREATE TABLE vendeurs (
  vd_id    INTEGER PRIMARY KEY AUTO_INCREMENT,
  vd_name  TEXT NOT NULL
) ENGINE=InnoDB;

CREATE TABLE recettes_vendeurs (
  vd_id       INTEGER NOT NULL REFERENCES vendeurs( vd_id ) ON DELETE RESTRICT,
  rc_date     DATE NOT NULL,
  rc_montant  NUMERIC( 12, 2 ),
  PRIMARY KEY( vd_id, rc_date ),
  KEY( rc_date, vd_id )
) ENGINE=InnoDB;