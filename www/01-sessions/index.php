<?php 
session_start();

function test_presence() {
	// Tester la présence de la variable 'langage' dans la session
	if ( isset( $_SESSION['langage'] ) ) {
		echo 'langage existe dans la session et sa valeur est ' ;
		// Lecture de la variable de session 'langage'
		echo $_SESSION['langage'] ;
	} else {
		echo 'langage n\'existe pas dans la session' ;
	}
}
?>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Cours 01 Sessions</title>
</head>
<body>
<h2>Action</h2>
	<a href="index.php?mode=r">Lecture</a>
	<a href="index.php?mode=w">Ecriture</a>
	<a href="index.php?mode=u">Effacement</a>
	<a href="index.php?mode=d">Suppression</a>

	<?php 
	switch (isset($_GET['mode'])?$_GET['mode']:'') {
		case 'r':
			?>
	<h2>Résultat</h2>
			<p>
		Lecture<br>
		<?php 
		test_presence();
		?>
	</p>
	<h2>Code</h2>
	<pre>
	session_start();
	</pre>
	<?php 
		break;
	case 'w':
	?>
	<h2>Résultat</h2>
	<p>
		Ecriture<br>
		<?php
		// Écrire 'PHP' dans la variable de session 'langage'
		$_SESSION['langage'] = 'PHP' ;
		$tableau = array('un', 'deux', 'trois', 'quatre');
		$_SESSION['tab'] = $tableau;

		test_presence();
		?>
	</p>
	<h2>Code</h2>
	<pre>
	session_start();
	
	// Écrire 'PHP' dans la variable de session 'langage'
	$_SESSION['langage'] = 'PHP' ;
	$tableau = array('un', 'deux', 'trois', 'quatre');
	$_SESSION['tab'] = $tableau;
	</pre>
	<?php 
		break;
	case 'u':
	?>
	<h2>Résultat</h2>
	<p>
		Effacement<br>
		<?php
		// On efface la session
		session_unset($_SESSION);

		test_presence();
		?>
	</p>
	<h2>Code</h2>
	<pre>
	session_start();
	
	// On efface la session
	session_unset($_SESSION);
	</pre>
	<?php 
		break;
	case 'd':
	?>
	<h2>Résultat</h2>
	<p>
		Suppression<br>
		<?php 
		// On détruit la session
		session_destroy();

		test_presence();
		?>
	</p>
	<h2>Code</h2>
	<pre>
	session_start();
	
	// On détruit la session
	session_destroy();
	</pre>
		<?php 
		break;
	default:
		break;
	}
	?>
</body>
</html>
