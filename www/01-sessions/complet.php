<?php 
session_start();
?>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Cours 01 Sessions</title>
</head>
<body>
<p>Etape 1<br>
<?php 
// Tester la présence de la variable 'langage' dans la session
if ( isset( $_SESSION['langage'] ) ) {
	echo 'langage existe dans la session et sa valeur est ' ;
	// Lecture de la variable de session 'langage'
	echo $_SESSION['langage'] ;
} else {
	echo 'langage n\'existe pas dans la session' ;
}
?>
</p>
<p>Etape 2<br>
<?php
// Écrire 'PHP' dans la variable de session 'langage'
$_SESSION['langage'] = 'PHP' ;
$tableau = array('un', 'deux', 'trois', 'quatre');
$_SESSION['tab'] = $tableau;

// Tester la présence de la variable 'langage' dans la session
if ( isset( $_SESSION['langage'] ) ) {
	echo 'langage existe dans la session et sa valeur est ' ;
	// Lecture de la variable de session 'langage'
	echo $_SESSION['langage'] ;
} else {
	echo 'langage n\'existe pas dans la session' ;
}
?>
</p>
<p>Etape 3<br>
<?php // On détruit la session
session_unset($_SESSION);
session_destroy();

// Tester la présence de la variable 'langage' dans la session
if ( isset( $_SESSION['langage'] ) ) {
	echo 'langage existe dans la session et sa valeur est ' ;
	// Lecture de la variable de session 'langage'
	echo $_SESSION['langage'] ;
} else {
	echo 'langage n\'existe pas dans la session' ;
}
?>
</body>
</html>