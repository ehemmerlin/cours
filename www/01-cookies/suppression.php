<?php
// Si le cookie compteur de visite existe,
if ( isset( $_COOKIE['langage'] ) ) {
// on demande au navigateur d’effacer son cookie
setcookie('langage') ;
// et on en efface la valeur en local pour éviter
// de l’utiliser par erreur dans la suite de notre script
unset($_COOKIE['langage']) ;
}
?>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Cours 01 Cookies</title>
</head>
<body>
<p>Un cookie a été effacé</p>
<p>Son nom est : langage</p>
<p>
<?php 
echo isset($_COOKIE['langage']) ? 'Le cookie langage vaut : '.$_COOKIE['langage'].'<br>' : 'Le cookie langage n\'existe plus';
?>
</p>
</body>
</html>