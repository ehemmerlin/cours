<?php
// Attention :
// aucun texte HTML ne doit être envoyé avant le cookie.
setcookie('langage', 'PHP version 5') ;
//setcookie('langage', 'PHP version 5', mktime(0,0,0,12,31,2027)) ;
?>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Cours 01 Cookies</title>
</head>
<body>
<p>Un cookie a été envoyé</p>
<p>Son nom est : langage</p>
<p>Son contenu est : PHP version 5</p>
<p>
<?php 
echo 'Le cookie langage vaut : '.$_COOKIE['langage'].'<br>'; // génere une erreur car le cookie n'est pas accessible avant rechargement de la page
// echo isset($_COOKIE['langage']) ? 'Le cookie langage vaut : '.$_COOKIE['langage'].'<br>' : 'Le cookie langage n\'est pas encore accessible'; // Correction
?>
</p>
</body>
</html>