<?php
// On récupère la liste actuelle
if ( isset( $_COOKIE['liste'] ) ) {
// Si le cookie existe ,
// on récupère la liste des heures de visite
$liste_serialisee = $_COOKIE['liste'] ;
// Il faut maintenant décoder le contenu
// pour obtenir le tableau
$liste_tableau = unserialize( $liste_serialisee ) ;
}else{
// Si le cookie n’existe pas encore,
// la liste est un tableau vide
$liste_tableau = array() ;
}
// On ajoute l’heure actuelle
$liste_tableau[] = time() ;
// On renvoie le cookie avec sa nouvelle valeur
// Pour cela, on sérialise le tableau avant
$liste_serialisee = serialize( $liste_tableau ) ;
setcookie('liste', $liste_serialisee) ;
?>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Cours 01 Cookies</title>
</head>
<body>
<p> vous êtes venu
<?php echo count( $liste_tableau ) ; ?>
fois, voici le détail : </p>
<ul>
<?php foreach( $liste_tableau as $heure ) {
echo '<li>le ',date("d/m/Y H:i:s:", $heure),'</li>';
} ?>
</ul>
</body>
</html>