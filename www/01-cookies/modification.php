<?php
$message = array() ;
if (! isset($_COOKIE['langage']) ) {
setcookie('langage', 'PHP verison 5', mktime(0,0,0,12,31,2037) ) ;
} else {
// On met le cookie à jour
setcookie('langage', 'PHP version 6', mktime(0,0,0,12,31,2037) ) ;
}
?>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Cours 01 Cookies</title>
</head>
<body>
<?php 
echo isset($_COOKIE['langage']) ? 'Le cookie langage vaut : '.$_COOKIE['langage'].'<br>' : 'Le cookie langage n\'existe plus';
?>
</body>
</html>