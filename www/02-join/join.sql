-- Affichage des données jointes dans une table
-- Introdution aux outils suivants :
--   INNER JOIN, LEFT JOIN


-- Création de la base de donnée cours
CREATE DATABASE IF NOT EXISTS cours;

-- Placement sur la base de donnée cours
USE cours;

-- Suppression de la table client
DROP TABLE IF EXISTS client;

-- Suppression de la table categorie
DROP TABLE IF EXISTS categorie;

-- Création de la table client
CREATE TABLE IF NOT EXISTS client (
    cli_id MEDIUMINT UNSIGNED NOT NULL AUTO_INCREMENT,
    cli_nom VARCHAR(30) NOT NULL,
    cli_prenom VARCHAR(30) NOT NULL,
    cli_cat_id MEDIUMINT UNSIGNED,
    cli_adresse VARCHAR(60) NOT NULL,
    cli_mail VARCHAR(50) NULL DEFAULT 'pas de mail',
    cli_sexe CHAR(1) NULL,
    cli_date DATE NULL DEFAULT NULL,
    PRIMARY KEY (cli_id),
    KEY cli_cat_id (cli_cat_id)
)  ENGINE=InnoDB DEFAULT CHARACTER SET=utf8 COLLATE = utf8_general_ci; -- ENGINE=InnoDB -- ENGINE=MEMORY -- ENGINE=MyISAM

-- Création de la table categorie
CREATE TABLE IF NOT EXISTS categorie (
    cat_id MEDIUMINT UNSIGNED NOT NULL AUTO_INCREMENT,
    cat_nom VARCHAR(30) NOT NULL,
    PRIMARY KEY (cat_id)
)  ENGINE=InnoDB DEFAULT CHARACTER SET=utf8 COLLATE = utf8_general_ci; -- ENGINE=InnoDB -- ENGINE=MEMORY -- ENGINE=MyISAM

ALTER TABLE `client`
  ADD CONSTRAINT `client_fk_1` FOREIGN KEY (`cli_cat_id`) REFERENCES `categorie` (`cat_id`) ON DELETE CASCADE ON UPDATE CASCADE;

-- Insertion de données sans préciser le nom des colonnes
INSERT INTO categorie VALUES (NULL,'boxe'), (NULL,'gymnastique'), (NULL,'judo'), (NULL,'tennis'), (NULL,'équitation');

-- Affichage des données insérées
SELECT 
    *
FROM
    categorie ; -- ORDER BY cli_categorie;

-- Insertion de données sans préciser le nom des colonnes
INSERT INTO client VALUES (1,'Nadal', 'Rafael', 4, '21 rue du Grand Chelem 75000 PARIS', 'r.nadal@gmail.com', 'M', NULL);

-- Insertion de données multiples
INSERT INTO `cours`.`client` (`cli_nom`, `cli_prenom`, `cli_adresse`,`cli_cat_id`, `cli_sexe`) 
VALUES ('Bolelli', 'Simone', '5 rue de la Raquette 75000 PARIS', 4, 'M'),
('Douillet', 'David', '5 rue du Poids Lourd 75000 PARIS',3, 'M'),
('Makarov', 'Vitali', '5 rue du Poids Léger 75000 PARIS',NULL,'M');

-- Affichage des données jointes
SELECT 
    *
FROM
    client,
    categorie
WHERE
    client.cli_cat_id = categorie.cat_id; -- ORDER BY cli_categorie;

-- Affichage des données jointes
SELECT 
    *
FROM
    client
        LEFT JOIN
    categorie ON client.cli_cat_id = categorie.cat_id
WHERE
    categorie.cat_nom LIKE 'tenn%';

-- Affichage des données jointes
SELECT 
    *
FROM
    client
        INNER JOIN
    categorie ON client.cli_cat_id = categorie.cat_id;

-- Affichage des données jointes
SELECT 
    *
FROM
    client
        LEFT JOIN
    categorie ON client.cli_cat_id = categorie.cat_id;

-- Affichage des données jointes
SELECT 
    *
FROM
    categorie
        LEFT JOIN
    client ON client.cli_cat_id = categorie.cat_id;