<?php 
require_once "member.php";
?>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
</head>
<body>
<?php 
echo Member::$numMembers . "<br>";  // Displays "0"
$Member1 = new Member( "fred", "Chicago", "http://example.com/", Member::MEMBER );
$Member1->showProfile();
echo Member::$numMembers . "<br>";  // Displays "1"
$Member2 = new Member( "mary", "Chicago", "http://example.com/", Member::ADMINISTRATOR );
$Member2->showProfile();
echo Member::$numMembers . "<br>";  // Displays "2"
?>
</body>
</html>