<?php
/**
 * Classe Member
 * @author HB
 *
 */
class Member
{
	const MEMBER = 1;
	const MODERATOR = 2;
	const ADMINISTRATOR = 3;
	
	/**
	 * Attribut nom du membre
	 * @var string
	 */
	public $username = "";

	/**
	 * Attribut de localisation
	 * @var string
	 */
	private $location;
	/**
	 * Attribut de page internet
	 * @var string
	 */
	private $homepage;
	
	/**
	 * Nombre de membres
	 * @var int
	 */
	public static $numMembers = 0;
	
	/**
	 * Niveau
	 * @var int
	 */
	private $level;
	
	/**
	 * Constructeur
	 * @param unknown_type $username
	 * @param unknown_type $location
	 * @param unknown_type $homepage
	 */
	public function __construct( $username, $location, $homepage, $level ) {
		$this->username = $username;
		$this->location = $location;
		$this->homepage = $homepage;
		$this->level = $level;
		self::$numMembers++;
	}
	 
	/**
	 * Destructeur
	 */
	public function __destruct() {
		echo "Je vais disparaître";
	}
	
	/**
	 * Affiche le profile
	 */
	public function showProfile() {
		echo "<dl>";
		echo "<dt>Username:</dt><dd>$this->username</dd>";
		echo "<dt>Location:</dt><dd>$this->location</dd>";
		echo "<dt>Homepage:</dt><dd>$this->homepage</dd>";
		echo "<dt>Level:</dt><dd>".$this->getLevel()."</dd>";
		echo "</dl>";
	}
	
	/**
	 * Retourne le niveau
	 */
	public function getLevel() {
		if ( $this->level == self::MEMBER ) return "membre";
		if ( $this->level == self::MODERATOR ) return "moderateur";
		if ( $this->level == self::ADMINISTRATOR ) return "administrateur";
		return "inconnu";
	}
	
	/**
	 * Attribut indiquant si le membre est loggé
	 * @var bool
	 */
	private $loggedIn = false;

	/**
	 * Méthode loggin()
	 */
	public function login() {
		$this->loggedIn = true;
	}
	
	/**
	 * Méthode logout
	 */
	public function logout() {
		$this->loggedIn = false;
	}
	
	/**
	 * Méthode isLoggedIn
	 */
	public function isLoggedIn() {
		return $this->loggedIn;
	}
}
?>