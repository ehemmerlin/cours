<?php
class Topic {

	private $member;
	private $subject;

	public function __construct( $member, $subject ) {
		$this->member = $member;
		$this->subject = $subject;
	}

	public function getSubject() {
		return $this->subject;
	}

	public function __call( $method, $arguments ) {
		return $this->member->$method( $arguments );
	}
}
?>