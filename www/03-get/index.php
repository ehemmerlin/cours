<?php 
require_once "member.php";
require_once "topic.php";
?>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
</head>
<body>
<h1>Etape 1</h1>
<?php 
$aMember = new Member();
$aMember->username = "fred";
$aMember->location = "San Francisco";
echo $aMember->username . "<br>";  // Displays "fred"
echo $aMember->location . "<br>";  // Displays "San Francisco"
?>
<h1>Etape 2</h1>
<?php 
$aTopic = new Topic( $aMember, "Hello everybody!" );
echo $aTopic->getSubject() . "<br>";     // Displays "Hello everybody!"
echo $aTopic->getUsername() . "<br>";    // Displays "fred"
?>
</body>
</html>