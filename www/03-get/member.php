<?php
/**
 * Classe Member
 * @author HB
 *
 */
class Member
{
	private $username;
	private $data = array();

	public function __get( $property ) {
		if ( $property == "username" ) {
			return $this->username;
		} else {
			if ( array_key_exists( $property, $this->data ) ) {
				return $this->data[$property];
			} else {
				return null;
			}
		}
	}

	public function __set( $property, $value ) {
		if ( $property == "username" ) {
			$this->username = $value;
		} else {
			$this->data[$property] = $value;
		}
	}
	
	public function getUsername() {
		return $this->username;
	}
}
?>