<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Cours 01 Formulaire</title>
</head>
<body>
	<?php
	// Equivalent à if else
	$champ_saisie = isset($_POST["Champ_saisie"]) ? $_POST["Champ_saisie"] : '';
/*	
	if(isset($_POST["Champ_saisie"])) {
		$champ_saisie = $_POST["Champ_saisie"];
	} else {
		$champ_saisie = '';
	}
*/	
	$liste_choix = isset($_POST["Liste_Choix"]) ? $_POST["Liste_Choix"] : 'aucune';
	$zone_texte = isset($_POST["Zone_Texte"]) ? $_POST["Zone_Texte"] : '';
	$case_radio = isset($_POST["Case_Radio"]) ? $_POST["Case_Radio"] : 'aucune';
	
	$resultat = "Champ saisie = $champ_saisie <br>";
	$resultat .= 'Liste choix = ' . $liste_choix . '<br>';
	$resultat .= "Zone texte = " . $zone_texte . "<br>";

	if (isset($_POST["Case_Cocher"])) {
		for ($i = 0; $i < count($_POST["Case_Cocher"]); $i++)
		{
			$resultat .= $_POST["Case_Cocher"][$i] . "<br>";
		}
	}
	$resultat .= "Case radio = " . $case_radio . "<br>";
	echo $resultat;
	?>
</body>
</html>
