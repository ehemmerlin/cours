<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Cours 01 Chaine</title>
</head>
<body>
	<p>
		Etape 1 <br>
		<?php
		// Accéder à un caractère précis
		$texte = 'PHP' ;
		echo $texte{1} ; // Affiche H
		echo $texte[2] ; // Affiche P
		?>
	</p>
	<p>
		Etape 2 <br>
		<?php
		// Valeur ASCII d'un caractère
		echo ord('a') ; // Renvoie 97
		echo chr(97) ; // Renvoie a
		?>
	</p>
	<p>
		Etape 3 <br>
		<?php
		// Taille d'une chaine
		$livre = 'PHP 5' ;
		echo strlen($livre) ; // Affiche 5
		?>
	</p>
	<p>
		Etape 4 <br>
		<?php
		// Nombre de mots d'une chaine
		$livre = 'PHP 5' ;
		echo str_word_count($livre,0,'1234567890') ; // Affiche 2
		print_r(str_word_count($livre, 1, '1234567890')) ; // Affiche Array ( [0] => PHP [1] => 5 ) 
		?>
	</p>
	<p>
		Etape 5 <br>
		<?php
		// Position d'une sous chaine
		$pos = strpos('ehemmerlin@humanbooster.com', '@') ;
		if($pos === FALSE) {
			echo 'Le caractère @ n\'est pas présent';
		} else {
			echo "Le caractère @ est présent à la position $pos";
		}
		?>
	</p>
	<p>
		Etape 6 <br>
		<?php
		// Présence de certaines chaines
		// La fonction strspn() retourne la longueur de la première sous-chaîne contenant uniquement
		// les caractères spécifiés. La fonction strcspn() fait l’opération inverse et retourne la
		// longueur de la première sous-chaîne ne contenant aucun des caractères spécifiés.
		$chaine = "chaîne à vérifier" ;
		$masque = "'" ;
		if( strcspn($chaine, $masque) == strlen($chaine) ) {
			echo 'il n y a pas d\'apostrophes' ;
		} else {
			echo 'il y a des apostrophes' ;
		}
		?>
	</p>
	<p>
		Etape 7 <br>
		<?php
		// Protection
		// La fonction addslashes() permet de protéger automatiquement les guillemets, apostrophes et
		// barres obliques inverses en les préfixant automatiquement.
		$nom = "HUBERT d'URT";
		$prenom = "Pierre";
		$nom = addslashes($nom);
		$prenom = addslashes($prenom);
		//$sql = "INSERT INTO user (prenom, nom) VALUES ('$prenom', $nom)";
		// Sans l’appel à addslashes() l’apostrophe du nom
		// aurait pu provoquer une erreur.
		?>
	</p>
	<p>
		Etape 8 <br>
		<?php
		// Protection
		// La fonction htmlspecialchars() convertit les caractères spéciaux (<, > et &)
		// en entités (&lt;, &gt; et &amp;).
		$texte = "valeur avec & <br> et avec \" et '" ;
		// Ne convertit rien, tout est interprété
		echo $texte , "<br>\n" ;
		// Convertit les caractères &, >, < et "
		echo htmlspecialchars($texte) , "<br>\n ";
		echo htmlspecialchars ($texte, ENT_COMPAT) , "<br>\n ";
		// Convertit les caractères &, >, <, " et '
		echo htmlspecialchars ($texte, ENT_QUOTES), "<br>\n " ;
		// Convertit les caractères &, > et < uniquement
		echo htmlspecialchars ($texte, ENT_NOQUOTES) , "<br>\n ";
		?>
	</p>
	<p>
		Etape 9 <br>
	</p>
	<pre><?php
		// Prise en compte des caractères accentués
		echo 'Nombre de caractères de é = '.strlen("é")." (strlen)\n";
		echo 'Nombre de caractères de e = '.strlen("e")." (strlen)\n";
		echo 'Nombre de caractères de é = '.strlen("é")." (strlen)\n";
		echo 'Nombre de caractères de é = '.mb_strlen("é","UTF-8")." (mb_strlen)\n";
		?>
	</pre>
	<p>
		Etape 10 <br>
	</p>
	<pre><?php
		// Conversion de caractères
		$chaine = "TransFOrMEZ cEtTe ChAINE écrite";
		echo ucwords(strtolower($chaine))."\n";
		echo mb_convert_case($chaine,MB_CASE_TITLE,"UTF-8");
		?>
	</pre>
	<p>
		Etape 11 <br>
		<?php
		// La fonction strstr() fonctionne de manière similaire à strpos() mais retourne le reste de
		// la chaîne à partir de la position repérée, au lieu de la position elle-même. Elle a ses équivalents
		// stristr() pour l’analyse sans prise en compte de la casse, et strrchr() pour
		// l’analyse de droite à gauche.

		echo strpos('ehemmerlin@humanbooster.com', '@') ; // Renvoie 10
		echo ' : ';
		echo strstr('ehemmerlin@humanbooster.com', '@') ; // Renvoie @humanbooster.com
		?>
	</p>
	<p>
		Etape 12 <br>
		<?php
		// La fonction substr() prend en argument une chaîne de caractères référence, une
		// position de départ et une longueur. Cette fonction renvoie les caractères à partir de la
		// position initiale jusqu’à atteindre la longueur définie.
		$texte = 'PHP 5 avec plaisir' ;
		echo substr($texte, 6, 2) ; // Renvoie av
		?>
	</p>
	<p>
		Etape 13 <br>
		<?php
		// La fonction str_replace() permet de remplacer un motif dans une chaîne. Le premier
		// argument est la sous-chaîne à rechercher, le deuxième argument est la chaîne de remplacement,
		// et le troisième est la chaîne de référence où faire le remplacement.
		$texte = 'PHP 4 avec plaisir' ;
		$cherche = '4' ;
		$remplace = '5' ;
		echo str_replace($cherche, $remplace, $texte) ; // Renvoie PHP 5 avec plaisir
		?>
	</p>
	<p>
		Etape 14 <br>
		<?php
		//La fonction trim() retire les caractères blancs avant et après la chaîne de référence.
		$texte = '  PHP 5 avec plaisir  ' ;
		echo strlen($texte) ; // Renvoie 22
		$texte = trim($texte) ;
		echo ' - '.$texte.' - ';
		echo strlen($texte) ; // Renvoie 18, on a enlevé 4 espaces
		?>
	</p>
	<p>
		Etape 15 <br>
		<?php
		// La fonction str_pad() permet de compléter une chaîne jusqu’à une certaine longueur.
		str_pad('', 10) ; // Complète jusqu’à 10 caractères avec des espaces
		?>
	</p>
	<p>
		Etape 16 <br>
		<?php
		$texte = 'Avec des MAJSCULES et minuscules';
		echo strtoupper($texte);
		// Renvoie AVEC DES MAJUSCULES ET MINUSCULES
		echo strtolower($texte);
		// Renvoie avec des majuscules et minuscules
		?>
		<?php
		$texte = "Avec des majuscules et minuscules" ;
		echo ucfirst($texte) ;
		// Renvoie Avec des majuscules et minuscules
		echo ucwords($texte) ;
		// Renvoie Avec Des Majuscules Et Minuscules
		?>
	</p>
	<p>
		Etape 17 <br>
		<?php
		//La fonction wordwrap() permet d’opérer cette coupure de manière automatisée, et en
		//respectant l’intégrité des mots (pas de coupures au milieu d’un mot, il revient entièrement à
		//la ligne).
		$texte = 'Avec des majuscules et minuscules' ;
		echo wordwrap($texte,5,'<br>') ;
		?>
	</p>
	<p>
		Etape 18 <br>

	</p>
	<p>
		Etape 19 <br>

	</p>
	<p>
		Etape 20 <br>

	</p>
	<p>
		Etape 8 <br>

	</p>
</body>
</html>
