-- Création de la base de donnée cours
CREATE DATABASE IF NOT EXISTS cours;

-- Placement sur la base de donnée cours
USE cours;

-- Suppression de la table client
DROP TABLE IF EXISTS client;

-- Création de la table client
CREATE TABLE IF NOT EXISTS client (
    cli_id MEDIUMINT UNSIGNED NOT NULL AUTO_INCREMENT,
    cli_nom VARCHAR(30) NOT NULL,
    cli_prenom VARCHAR(30) NOT NULL,
    cli_categorie VARCHAR(40) NULL DEFAULT NULL, -- collate utf8_general_ci
    cli_adresse VARCHAR(60) NOT NULL,
    cli_mail VARCHAR(50) NULL DEFAULT 'pas de mail',
    cli_sexe CHAR(1) NULL,
    cli_date DATE NULL DEFAULT NULL,
    PRIMARY KEY (cli_id)
)  ENGINE=InnoDB DEFAULT CHARACTER SET=utf8 COLLATE = utf8_general_ci; -- ENGINE=InnoDB -- ENGINE=MEMORY -- ENGINE=MyISAM

-- Insertion de données dans la table client
INSERT INTO `cours`.`client` (`cli_nom`, `cli_prenom`, `cli_adresse`,`cli_categorie`, `cli_mail`, `cli_sexe`) VALUES ('Nadal', 'Rafael', '21 rue du Grand Chelem 75000 PARIS', 'tennis', 'r.nadal@gmail.com', 'M');
INSERT INTO `cours`.`client` (`cli_nom`, `cli_prenom`, `cli_adresse`,`cli_categorie`, `cli_sexe`) VALUES ('Bolelli', 'Simone', '5 rue de la Raquette 75000 PARIS', NULL, 'M');
INSERT INTO `cours`.`client` (`cli_nom`, `cli_prenom`, `cli_adresse`,`cli_categorie`,`cli_date`, `cli_sexe`) VALUES ('Douillet', 'David', '5 rue du Poids Lourd 75000 PARIS','Judo','1969/02/17', 'M');
INSERT INTO `cours`.`client` (`cli_nom`, `cli_prenom`, `cli_adresse`,`cli_categorie`,`cli_date`) VALUES ('Makarov', 'Vitali', '5 rue du Poids Léger 75000 PARIS','judo','19740623');
INSERT INTO `cours`.`client` (`cli_nom`, `cli_prenom`, `cli_adresse`,`cli_categorie`,`cli_date`, `cli_sexe`) VALUES ('Makarov', 'Vitali', '5 rue du Poids Léger 75000 PARIS',NULL,'19740623','F');

-- Affichage des données insérées
SELECT 
    *
FROM
    client;

-- CONCAT
SELECT 1 + 2;
SELECT CONCAT(1,2); -- Concatène deux chaines ensemble

-- COLLATE 
SELECT * FROM client 
WHERE cli_nom COLLATE utf8_bin = 'Douillet'; -- Change la collation en dynamique

-- COALESCE
SELECT CONCAT(cli_nom, ' ', cli_categorie) -- Affiche NULL si l'un des champs est NULL
FROM Client;

SELECT CONCAT(cli_nom, ' ', IFNULL(cli_categorie, 'Inconnu')) -- Affiche Inconnu si l'une des valeurs du champs cli_categorie est NULL
FROM Client;

SELECT CONCAT(cli_nom, ' ', COALESCE(cli_categorie, cli_prenom, 'Inconnu')) -- Cascade si l'un est NULL
FROM Client;

-- Fonctions date CURDATE, CURRENT_TIMESTAMP, DATEDIFF, DATE_ADD
SELECT CURDATE();
SELECT CURRENT_TIMESTAMP;
SELECT cli_date,
  YEAR(cli_date) as Naissance,
	DATEDIFF(CURRENT_TIMESTAMP, cli_date) as "Difference",
	DATE_ADD(cli_date, INTERVAL 30 DAY) as Plus30j,
	DATE_ADD(cli_date, INTERVAL -30 DAY) as Moins30j,
	DATE_SUB(cli_date, INTERVAL 30 DAY) as Moinsj
FROM client;

-- Fonctions chaines de caractère
DROP VIEW cli;
CREATE VIEW cli AS (SELECT CONCAT(cli_prenom,' ',cli_nom) as cli_nom, DATEDIFF(CURRENT_TIMESTAMP, cli_date) as "Difference"  FROM client);
SELECT *
FROM cli;
SELECT cli_nom,
	INSTR(cli_nom, ' ') as res1, -- Recherche de la chaine de caractère
	LEFT(cli_nom, INSTR(cli_nom, ' ')- 1) as res2, -- Extrait par la gauche à la position demandée
	SUBSTRING(cli_nom, INSTR(cli_nom, ' ')+ 1, 100) as res3, -- Retourne un sous-ensemble d'une chaine de caractère
	LTRIM(SUBSTRING(cli_nom, INSTR(cli_nom, ' '), 100)) as res4 -- Enlève les espaces à gauche
FROM cli;

-- Fonctions numériques
SELECT 
	Difference,
	Difference / 3, 
	COALESCE(Difference/3, 0) * 3,
	CEILING(Difference/3),
	FLOOR(Difference/3),
	ROUND(Difference/3, 1)
FROM cli;

-- CASE

-- première syntaxe du CASE
SELECT 
	cli_prenom,
	cli_nom,
	CASE cli_categorie
		WHEN 'judo' THEN 'tatami'
		WHEN 'tennis'  THEN 'raquette'
		ELSE 'ne sait pas'
	END
FROM client;

-- deuxième syntaxe du CASE
SELECT 
	cli_prenom,
	cli_nom,
	CASE 
		WHEN cli_categorie IN ('tennis', 'ping-pong') THEN 'raquette'
		WHEN cli_categorie IN ('judo', 'aïkido')  THEN 'tatami'
		ELSE 'ne sait pas'
	END
FROM Client;
