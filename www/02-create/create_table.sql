-- Création d'une base, d'une table et insertion d'enregistrements
-- Introdution aux outils suivants :
--   CREATE DATABASE / CREATE TABLE
--   DROP DATABASE / DROP TABLE
--   IF EXISTS / IF NOT EXISTS
--   USE
--   ENGINE=InnoDB / ENGINE=MEMORY / ENGINE=MyISAM
--   collate utf8_general_ci / collate utf8_bin
--   NOT NULL / NULL
--   DEFAULT
--   SELECT
--   TRUE / FALSE / NULL
--   ORDER BY
--   WHERE
--   SHOW
--   DESCRIBE


-- Création de la base de donnée cours
CREATE DATABASE IF NOT EXISTS cours;

-- Placement sur la base de donnée cours
USE cours;

-- Suppression de la table client
DROP TABLE IF EXISTS client;

-- Création de la table client
CREATE TABLE IF NOT EXISTS client (
    cli_id MEDIUMINT UNSIGNED NOT NULL AUTO_INCREMENT,
    cli_nom VARCHAR(30) NOT NULL,
    cli_prenom VARCHAR(30) NOT NULL,
    cli_categorie VARCHAR(40) collate utf8_general_ci NULL DEFAULT NULL,
    cli_adresse VARCHAR(60) NOT NULL,
    cli_mail VARCHAR(50) NULL DEFAULT 'pas de mail',
    cli_sexe CHAR(1) NULL,
    cli_date DATE NULL DEFAULT NULL,
    PRIMARY KEY (cli_id)
)  ENGINE=InnoDB DEFAULT CHARACTER SET=utf8 COLLATE = utf8_general_ci; -- ENGINE=InnoDB -- ENGINE=MEMORY -- ENGINE=MyISAM

-- Insertion de données dans la table client
INSERT INTO `cours`.`client` (`cli_nom`, `cli_prenom`, `cli_adresse`,`cli_categorie`, `cli_mail`, `cli_sexe`) VALUES ('Nadal', 'Rafael', '21 rue du Grand Chelem 75000 PARIS', 'tennis', 'r.nadal@gmail.com', 'M');
INSERT INTO `cours`.`client` (`cli_nom`, `cli_prenom`, `cli_adresse`,`cli_categorie`, `cli_sexe`) VALUES ('Bolelli', 'Simone', '5 rue de la Raquette 75000 PARIS', 'Tennis', 'M');
INSERT INTO `cours`.`client` (`cli_nom`, `cli_prenom`, `cli_adresse`,`cli_categorie`,`cli_date`, `cli_sexe`) VALUES ('Douillet', 'David', '5 rue du Poids Lourd 75000 PARIS','Judo','1969/02/17', 'M');
INSERT INTO `cours`.`client` (`cli_nom`, `cli_prenom`, `cli_adresse`,`cli_categorie`,`cli_date`) VALUES ('Makarov', 'Vitali', '5 rue du Poids Léger 75000 PARIS','judo','19740623');

-- Affichage des données insérées
SELECT 
    *
FROM
    client ; -- ORDER BY cli_categorie;
    
-- Affichage des données insérées
SELECT 
    *
FROM
    client
WHERE
    cli_sexe = 'M' OR cli_sexe <> 'M'; -- OR cli_sexe IS NULL
    
-- Affiche toutes les tables créées dans la base
SHOW TABLES;

-- Détaille la structure de la table client
DESCRIBE client;

-- Détermine quel moteur gère la table
SHOW TABLE STATUS FROM cours;

-- Modifie le moteur de la table
ALTER TABLE client ENGINE=MyISAM;

-- Détermine quel moteur gère la table
SHOW TABLE STATUS FROM cours;